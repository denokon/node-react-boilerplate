import { Configuration, DefinePlugin } from "webpack";
import os from "os";
// import MiniCssExtractPlugin from "mini-css-extract-plugin";
import path from "path";

import HtmlWebpackPlugin from "html-webpack-plugin";
import nodeExternals from "webpack-node-externals";
import CopyPlugin from "copy-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";
import CircularDependencyPlugin from "circular-dependency-plugin";
// @ts-ignore
import Dotenv from "dotenv-flow-webpack";
import findUp from "find-up";

import ReactRefreshWebpackPlugin from "@pmmmwh/react-refresh-webpack-plugin";

process.env["TS_NODE_PROJECT"] = "";

export interface ConfigFactoryOptions {
  htmlOptions?: HtmlWebpackPlugin.Options;
  outputOptions: Configuration["output"];
  entry?: string;
  useSvgLoader?: boolean;
  isTargetingNode?: boolean;
  useSourcemap?: boolean;
}

export interface ConfigEnvironmentVariables {
  useIp?: string | boolean;
  NODE_ENV?: string;
  production?: boolean;
  reactDevTools?: boolean;
  analyzer?: boolean;
  useSourcemap?: boolean;
}

export function getConfigFactory(
  factoryOptions: ConfigFactoryOptions | ((env: ConfigEnvironmentVariables) => ConfigFactoryOptions)
) {
  return function configFactory(envs: ConfigEnvironmentVariables = {}) {
    for (const [key] of Object.entries(envs)) {
      if (key.includes("=")) {
        const [actualkey, actualValue] = key.split("=");
        (envs as Record<string, string>)[actualkey] = actualValue;
      }
    }

    let {
      useIp = false,
      // eslint-disable-next-line prefer-const
      NODE_ENV = "dev",
      // eslint-disable-next-line prefer-const
      production = false,
      // eslint-disable-next-line prefer-const
      reactDevTools = false,
      // eslint-disable-next-line prefer-const
      analyzer = false,
      // eslint-disable-next-line prefer-const
      useSourcemap: useSourcemapEnv,
    } = envs;

    const env = { useIp, NODE_ENV, production, reactDevTools, analyzer };

    const { htmlOptions, outputOptions, useSvgLoader, isTargetingNode, entry, useSourcemap: useSourcemapOptions } =
      typeof factoryOptions === "function" ? factoryOptions(env) : factoryOptions;

    const useSourcemap = useSourcemapEnv ?? useSourcemapOptions;

    if (useIp === true) {
      breakLoop: for (const networkInterfaces of Object.values(os.networkInterfaces())) {
        if (!networkInterfaces) continue;

        for (const networkInterface of networkInterfaces) {
          if (
            networkInterface.internal ||
            networkInterface.family !== "IPv4" ||
            !networkInterface.address.startsWith("192.168.")
          )
            continue;

          useIp = networkInterface.address;
          console.warn("Using local IP: " + useIp);

          break breakLoop;
        }
      }

      if (useIp === true) throw new Error(`Could not determine local ip`);
    }

    const babelLoader = {
      loader: "babel-loader",
      options: {
        // cacheDirectory: true,
        envName: production ? "production" : "development",
      },
    };

    const publicPath = outputOptions?.publicPath as string | undefined;

    const config: Configuration = {
      mode: production ? "production" : "development",
      entry: entry ?? "./src/index.tsx",
      // Currently we need to add '.ts' to the resolve.extensions array.
      resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"],
        alias: {
          "@my-app/common/src": path.resolve("../common/src"),
          "@my-app/common": path.resolve("../common/src"),
        },
        // "esm2017" is added so we import the smaller version of firebase/firestore
        // FIXME: Currently firestore esm2017 is buggy, so restore this when it gets fixed
        // mainFields: ["esm2017", "browser", "module", "main"],
        // plugins: [new TsconfigPathsPlugin()]
        fallback: {
          querystring: false,
        },
      },
      output: outputOptions,
      devServer: {
        publicPath,
        stats: "minimal",
        host: (useIp as string) ?? undefined,
        historyApiFallback: {
          index: publicPath ? publicPath.replace(/\/+$/, "") + "/index.html" : "/index.html",
        },
        hot: true,
        overlay: {
          warnings: true,
          errors: true,
        },
        compress: true,
      },
      stats: "minimal",

      // Add the loader for .ts files.
      module: {
        rules: [
          {
            test: /react-spring/,
            sideEffects: true,
          },
          {
            test: /\.tsx?$/,
            exclude: /node_modules|\.yarn/,
            use: [babelLoader],
          },
        ],
      },
      plugins: [
        new CircularDependencyPlugin({
          exclude: /node_modules/,
          failOnError: true,
        }),
        new DefinePlugin({
          CONNECT_REACT_DEVTOOLS: reactDevTools.toString(),
          HREF_BASE_NAME: JSON.stringify(outputOptions?.publicPath ?? "/"),
          "process.env.PRODUCTION": JSON.stringify(production),
          "process.env.NODE_ENV": JSON.stringify(NODE_ENV),
        }),
        new Dotenv({
          node_env: NODE_ENV,
          path: path.dirname(findUp.sync([".env.local", ".env." + NODE_ENV, ".env"]) ?? "./.env"),
        }),
        new CleanWebpackPlugin(),
      ],
    };

    if (!config.module || !config.plugins || !config.resolve || !config.resolve.alias) {
      throw new Error("Something went seriously wrong.");
    }

    if (useSourcemap) {
      config.devtool = "source-map";
    }

    if (isTargetingNode) {
      config.target = "node";
      config.devtool = "source-map";
      config.externals = [
        nodeExternals({
          allowlist: [/lodash-es/],
        }),
        nodeExternals({
          allowlist: [/lodash-es/],
          modulesDir: path.resolve(__dirname, "node_modules"),
        }),
      ];
      config.plugins.unshift(
        new CopyPlugin({
          patterns: [
            {
              from: "package.json",
              to: "package.json",
              transform(content: string | Buffer) {
                if (content instanceof Buffer) content = content.toString();

                // Remove scripts, so postinstall doesn't run on the server
                const { scripts, ...parsedContent } = JSON.parse(content);

                return JSON.stringify(parsedContent);
              },
            },
            "../../yarn.lock",
          ],
        })
      );
    } else {
      config.module.rules?.push(
        {
          test: /\.css$/,
          use: [
            /*production ? MiniCssExtractPlugin.loader :*/ "style-loader",
            { loader: "css-loader", options: { sourceMap: true } },
          ],
        },
        {
          test: useSvgLoader
            ? /\.(png|jpg|gif|webp|ico|eot|ttf|woff2?|webm|mp4)$/
            : /\.(png|(?<!inline\.)svg$|jpg|gif|webp|ico|eot|ttf|woff2|mp4|webm?)$/,
          use: ["file-loader"],
        }
      );

      config.plugins.unshift(new HtmlWebpackPlugin({ ...htmlOptions, environment: { useIp } }));

      if (production) {
        // config.plugins.push(new MiniCssExtractPlugin());

        if (analyzer) {
          config.plugins.push(new BundleAnalyzerPlugin());
        }
      } else {
        config.plugins.push(new ReactRefreshWebpackPlugin());

        if (!isTargetingNode && config.output) {
          delete config.output.filename;
        }
      }

      if (useSvgLoader) {
        config.module.rules?.push({
          test: /\.svg$/,
          use: [
            babelLoader,
            {
              loader: "react-svg-loader",
              options: {
                jsx: true,
              },
            },
          ],
        });
      }
    }

    return config;
  };
}
