import path from "path";
import { getConfigFactory } from "../../webpack.config";

export default getConfigFactory({
  outputOptions: {
    filename: "index.js",
    path: path.resolve("dist"),
  },
  isTargetingNode: true,
});
