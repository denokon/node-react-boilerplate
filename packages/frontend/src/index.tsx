import { App } from "./components/app";
import { render } from "react-dom";

const appElement = document.createElement("div");
appElement.id = "app-container";
appElement.classList.add("app");
document.body.appendChild(appElement);

render(<App />, appElement);
