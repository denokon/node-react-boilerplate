import React from "react";
import styled from "styled-components";

export function App() {
  return <StyledApp>Hello there</StyledApp>;
}

const StyledApp = styled.div`
  font-weight: bold;
`;
