import { getConfigFactory } from "../../webpack.config";
import path from "path";

const appName = "My App";

export default getConfigFactory({
  htmlOptions: {
    title: appName,
    template: path.resolve("./src/index.html"),
  },
  outputOptions: {
    filename: "index.[contenthash].js",
    path: path.resolve("dist"),
    publicPath: "/",
  },
  // variables: {
  //   APP_NAME: appName
  // },
  useSvgLoader: true,
});
