#!/usr/bin/env sh

# Replace occurrences of {{BACKEND_URL}} with the value of the $BACKEND_URL env variable
find /usr/share/nginx/html/* -maxdepth 1 -type f -exec sed -i -- "s/{{BACKEND_URL}}/$BACKEND_URL/g" {} \;