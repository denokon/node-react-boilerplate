/* eslint-env node */
module.exports = function (api) {
  const isProduction = api.env("production");
  const isTargetingNode = api.caller(
    (caller) => caller && caller.target === "node"
  );

  const config = {
    presets: [
      [
        "@babel/preset-typescript",
        {
          isTSX: true,
          allExtensions: true,
          allowDeclareFields: true,
          onlyRemoveTypeImports: true,
        },
      ],
    ],
    plugins: [["@babel/plugin-proposal-class-properties", { loose: true }]],
  };

  if (!isTargetingNode) {
    config.presets.unshift(
      // Current useBuiltIns and useSpread a mutually exclusive. But looking at the source code
      // (https://github.com/babel/babel/blob/3d2f3650745d981127109871b8ffdbd34e28d87c/packages/babel-helper-builder-react-jsx/src/index.js#L193)
      // useSpread is the superior option
      [
        "@babel/preset-react",
        {
          /*useBuiltIns: true,*/ runtime: "automatic",
          useSpread: true,
          development: !api.env("production"),
        },
      ]
    );

    config.plugins.push(
      isProduction
        ? "babel-plugin-styled-components"
        : [
            "babel-plugin-styled-components",
            {
              displayName: true,
              minify: false,
              transpileTemplateLiterals: false,
              topLevelImportPaths: ["@my-app/common/themes/theme-types"],
            },
          ]
    );

    if (!isProduction) {
      config.plugins.push("react-refresh/babel");
    } else {
      config.presets.push([
        "@babel/preset-env",
        {
          targets: "last 1 firefox versions, last 1 chrome versions",
          loose: true,
          modules: false,
          useBuiltIns: "usage",
          corejs: { version: 3 },
          shippedProposals: true,
        },
      ]);
    }
  } else {
    config.presets.push([
      "@babel/preset-env", {
      targets: {
        node: true,
      },
      loose: true,
    }]);
  }

  api.cache.forever();

  return config;
};
